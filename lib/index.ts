import { NumericRange } from "@valuer/types";
import { brackets } from "@valuer/brackets";

/** @private */
const getPrintableType = (value: any): string => {
	if (value == null)
		return "void";

	else if (typeof value === "object")
		if (value.constructor != null)
			return (value.constructor as Function).name;

		else return "Object";

	else return typeof value;
};

/** @private */
const toRange = (range: number[]): NumericRange =>
	[ Math.min(...range), Math.max(...range) ];

export namespace help {
	/**
	 * Convert value to printable console-friendly string
	 * @arg value The value
	 */
	export const getPrintable = (value: any): string => {
		const type = brackets.surround(getPrintableType(value), brackets.collection.angular.tight);
		const _value = value instanceof Array? brackets.surround(value, brackets.collection.square.tight) : value;

		return `${ type } ${ _value }`;
	};

	/**
	 * Convert range to printable string
	 * @arg range The range
	 * @arg inclusively (defaults to `true`) Whether the boundary match is counted
	 */
	export const getPrintableRange = (range: NumericRange, inclusively: boolean = true): string =>
		brackets.surround(toRange(range).join(" to "), inclusively? brackets.collection.square.tight : brackets.collection.round.tight);
}
