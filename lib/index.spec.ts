import "mocha";
import { expect } from "chai";
import { help } from "./index";

describe('help.getPrintable', () => {
	it("shows up a value with its type information", () => {
		expect(help.getPrintable("some text")).to.equal("<string> some text");
		expect(help.getPrintable(null)).to.equal("<void> null");
		expect(help.getPrintable([ 17, 42 ])).to.equal("<Array> [17,42]");
	});
});

describe('help.getPrintableRange', () => {
	it("shows up a range of numbers", () => {
		expect(help.getPrintableRange([ 42, 17 ])).to.equal("[17 to 42]");
		expect(help.getPrintableRange([ 42, 17 ], false)).to.equal("(17 to 42)");
	});
});
